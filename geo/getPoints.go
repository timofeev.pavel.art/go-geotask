package geo

import (
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func ParsePoints() (map[string][]Point, error) {
	wd, err := os.Getwd()
	if err != nil {
		return nil, err
	}
	splt := strings.TrimSuffix(wd, "module/order/service")
	splt = strings.TrimSuffix(splt, "module/courier/service")
	file, err := os.ReadFile(fmt.Sprintf("%s/public/js/polygon.js", splt))
	if err != nil {
		return nil, err
	}

	str := regexp.MustCompile(`[^0-9,.\n]`).ReplaceAllString(string(file), "")

	strList := strings.Split(str, "\n")
	var temp []string
	for i := 0; i != len(strList); i++ {
		if len(strList[i]) == 0 {
			continue
		}
		temp = append(temp, strList[i])
	}

	strList = temp

	out := make(map[string][]Point, 3)
	out["allowed"] = make([]Point, 0)
	out["notAllowed1"] = make([]Point, 0)
	out["notAllowed2"] = make([]Point, 0)

	key := "allowed"
	for i := range strList {
		var num1, num2 float64

		if strList[i] == "1" {
			key = "notAllowed1"
			continue
		} else if strList[i] == "2" {
			key = "notAllowed2"
			continue
		}

		tmp := strings.Split(strList[i], ",")

		num1, err = strconv.ParseFloat(tmp[0], 64)
		if err != nil {
			return nil, err
		}

		num2, err = strconv.ParseFloat(tmp[1], 64)
		if err != nil {
			return nil, err
		}

		point := Point{
			Lat: num1,
			Lng: num2,
		}
		out[key] = append(out[key], point)
	}

	return out, err
}
