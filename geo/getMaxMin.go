package geo

func GetMax(arr interface{}) float64 {
	var floats []float64
	var flOut float64
	switch v := arr.(type) {
	case []float64:
		floats = v

		for _, v := range floats {
			if v > flOut {
				flOut = v
			}
		}
		return flOut
	}
	return 0
}

func GetMin(arr interface{}) float64 {
	var floats []float64
	var flOut float64
	switch v := arr.(type) {
	case []float64:
		flOut = v[0]
		floats = v
		for _, v := range floats {
			if v < flOut {
				flOut = v
			}
		}
		return flOut
	}

	return 0
}
