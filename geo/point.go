package geo

import (
	"math/rand"

	geo "github.com/kellydunn/golang-geo"
)

func (p *Polygon) Contains(point Point) bool {
	geoPoint := geo.NewPoint(point.Lat, point.Lng)
	return p.polygon.Contains(geoPoint)
}

func (p *Polygon) Allowed() bool {
	return p.allowed
}

func (p *Polygon) RandomPoint() Point {
	var (
		point          Point
		lats, lngs     []float64
		latMin, latMax float64
		lngMin, lngMax float64
	)
	polyPoints := p.polygon.Points()

	length := len(polyPoints)
	lats = make([]float64, length)
	lngs = make([]float64, length)

	for i, item := range polyPoints {
		lats[i] = item.Lat()
		lngs[i] = item.Lng()
	}

	latMax = GetMax(lats)
	latMin = GetMin(lats)

	lngMax = GetMax(lngs)
	lngMin = GetMin(lngs)

	point = Point{
		Lat: rand.Float64()*(latMax-latMin) + latMin,
		Lng: rand.Float64()*(lngMax-lngMin) + lngMin,
	}

	return point
}
