package storage

import (
	"context"
	"encoding/json"
	"time"

	"gitlab.com/timofeev.pavel.art/go-geotask/prometeus"

	"github.com/go-redis/redis/v8"
	"gitlab.com/timofeev.pavel.art/go-geotask/module/courier/models"
)

type CourierStorageRedis struct {
	storage *redis.Client
}

func NewCourierStorageRedis(storage *redis.Client) CourierStorager {
	return &CourierStorageRedis{storage: storage}
}

func (c *CourierStorageRedis) Save(ctx context.Context, courier models.Courier) error {
	var (
		err  error
		data []byte
	)
	start := time.Now()

	data, err = json.Marshal(courier)
	if err != nil {
		return err
	}
	err = c.storage.Set(ctx, "courier", data, 0).Err()
	if err != nil {
		return err
	}

	elapsed := time.Since(start)
	prometeus.RepositoryMethodsDuration.WithLabelValues("courier", "Save").Observe(float64(elapsed.Microseconds()))
	return nil
}

func (c *CourierStorageRedis) GetOne(ctx context.Context) (*models.Courier, error) {
	var (
		err     error
		courier models.Courier
		data    string
	)
	start := time.Now()

	data, err = c.storage.Get(ctx, "courier").Result()
	if err == redis.Nil {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	err = json.Unmarshal([]byte(data), &courier)
	if err != nil {
		return nil, err
	}

	elapsed := time.Since(start)
	prometeus.RepositoryMethodsDuration.WithLabelValues("courier", "GetOne").Observe(float64(elapsed.Microseconds()))

	return &courier, nil
}
