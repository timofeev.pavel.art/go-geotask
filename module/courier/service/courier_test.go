package service

import (
	"context"
	"reflect"
	"testing"

	"github.com/golang/mock/gomock"

	mock_storage "gitlab.com/timofeev.pavel.art/go-geotask/module/courier/storage/courier_service_mock"

	"gitlab.com/timofeev.pavel.art/go-geotask/geo"
	"gitlab.com/timofeev.pavel.art/go-geotask/module/courier/models"
)

var (
	alZones    = geo.NewAllowedZone()
	disalZones = []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()}
)

func TestCourierService_GetCourier(t *testing.T) {
	type fields struct {
		courierStorage *mock_storage.MockCourierStorager
		allowedZone    geo.PolygonChecker
		disabledZones  []geo.PolygonChecker
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		prepare func(f *fields)
		args    args
		want    *models.Courier
		wantErr bool
	}{
		{
			name: "GetCourier_test",
			prepare: func(f *fields) {
				f.courierStorage.EXPECT().GetOne(context.Background()).Return(
					&models.Courier{
						Score: 0,
						Location: models.Point{
							Lat: DefaultCourierLat,
							Lng: DefaultCourierLng,
						},
					},
					nil,
				)
				f.courierStorage.EXPECT().Save(context.Background(), models.Courier{
					Score: 0,
					Location: models.Point{
						Lat: DefaultCourierLat,
						Lng: DefaultCourierLng,
					},
				}).Return(nil)
			},
			args: args{ctx: context.Background()},
			want: &models.Courier{
				Score: 0,
				Location: models.Point{
					Lat: DefaultCourierLat,
					Lng: DefaultCourierLng,
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			f := fields{
				courierStorage: mock_storage.NewMockCourierStorager(ctrl),
				allowedZone:    alZones,
				disabledZones:  disalZones,
			}

			if tt.prepare != nil {
				tt.prepare(&f)
			}

			c := &CourierService{
				courierStorage: f.courierStorage,
				allowedZone:    f.allowedZone,
				disabledZones:  f.disabledZones,
			}
			got, err := c.GetCourier(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetCourier() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetCourier() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCourierService_MoveCourier(t *testing.T) {
	type fields struct {
		courierStorage *mock_storage.MockCourierStorager
		allowedZone    geo.PolygonChecker
		disabledZones  []geo.PolygonChecker
	}
	type args struct {
		courier   models.Courier
		direction int
		zoom      int
	}
	tests := []struct {
		name    string
		prepare func(f *fields)
		args    args
		wantErr bool
	}{
		{
			name: "MoveCourier_test",
			prepare: func(f *fields) {
				f.courierStorage.EXPECT().Save(context.Background(), gomock.Any()).Return(nil)
			},
			args: args{
				courier: models.Courier{
					Score: 0,
					Location: models.Point{
						Lat: DefaultCourierLat,
						Lng: DefaultCourierLng,
					},
				},
				direction: 0,
				zoom:      6,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			f := fields{
				courierStorage: mock_storage.NewMockCourierStorager(ctrl),
				allowedZone:    alZones,
				disabledZones:  disalZones,
			}

			if tt.prepare != nil {
				tt.prepare(&f)
			}

			c := &CourierService{
				courierStorage: f.courierStorage,
				allowedZone:    f.allowedZone,
				disabledZones:  f.disabledZones,
			}
			if err := c.MoveCourier(tt.args.courier, tt.args.direction, tt.args.zoom); (err != nil) != tt.wantErr {
				t.Errorf("MoveCourier() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestCourierService_UpdateCourier(t *testing.T) {
	type fields struct {
		courierStorage *mock_storage.MockCourierStorager
		allowedZone    geo.PolygonChecker
		disabledZones  []geo.PolygonChecker
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		prepare func(f *fields)
		args    args
		wantErr bool
	}{
		{
			name: "UpdateCourier_test",
			prepare: func(f *fields) {
				f.courierStorage.EXPECT().GetOne(context.Background()).Return(&models.Courier{
					Score: 0,
					Location: models.Point{
						Lat: DefaultCourierLat,
						Lng: DefaultCourierLng,
					},
				}, nil)
				f.courierStorage.EXPECT().Save(context.Background(), models.Courier{
					Score: 1,
					Location: models.Point{
						Lat: DefaultCourierLat,
						Lng: DefaultCourierLng,
					},
				}).Return(nil)
			},
			args:    args{ctx: context.Background()},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			f := fields{
				courierStorage: mock_storage.NewMockCourierStorager(ctrl),
				allowedZone:    alZones,
				disabledZones:  disalZones,
			}

			if tt.prepare != nil {
				tt.prepare(&f)
			}

			c := &CourierService{
				courierStorage: f.courierStorage,
				allowedZone:    f.allowedZone,
				disabledZones:  f.disabledZones,
			}
			if err := c.UpdateCourier(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("UpdateCourier() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
