package service

import (
	"context"
	"fmt"
	"reflect"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	mock_storage "gitlab.com/timofeev.pavel.art/go-geotask/module/order/storage/order_mock"

	"gitlab.com/timofeev.pavel.art/go-geotask/geo"
	"gitlab.com/timofeev.pavel.art/go-geotask/module/order/models"
)

var (
	alZones    = geo.NewAllowedZone()
	disalZones = []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()}
)

func TestOrderService_GenerateOrder(t *testing.T) {

	type fields struct {
		storage       *mock_storage.MockOrderStorager
		allowedZone   geo.PolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		prepare func(f *fields)
		args    args
		wantErr bool
	}{
		{
			name: "GenerateOrder_test",
			prepare: func(f *fields) {
				f.storage.EXPECT().Save(context.Background(), gomock.Any(), 2*time.Minute+3).Return(nil)
			},
			args:    args{ctx: context.Background()},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			f := fields{
				storage:       mock_storage.NewMockOrderStorager(ctrl),
				allowedZone:   alZones,
				disabledZones: disalZones,
			}

			if tt.prepare != nil {
				tt.prepare(&f)
			}
			o := &OrderService{
				storage:       f.storage,
				allowedZone:   f.allowedZone,
				disabledZones: f.disabledZones,
			}
			if err := o.GenerateOrder(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("GenerateOrder() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestOrderService_GetByRadius(t *testing.T) {
	orders := []models.Order{{
		ID:            1,
		Price:         123,
		DeliveryPrice: 123,
		Lng:           30.3600,
		Lat:           59.9310,
		IsDelivered:   false,
	},
		{
			ID:            2,
			Price:         321,
			DeliveryPrice: 321,
			Lng:           30.3605,
			Lat:           59.9316,
			IsDelivered:   false,
		},
	}

	type fields struct {
		storage       *mock_storage.MockOrderStorager
		allowedZone   geo.PolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx    context.Context
		lng    float64
		lat    float64
		radius float64
		unit   string
	}
	tests := []struct {
		name    string
		prepare func(f *fields)
		args    args
		want    []models.Order
		wantErr bool
	}{
		{
			name: "GetByRadius_test",
			prepare: func(f *fields) {
				f.storage.EXPECT().GetByRadius(context.Background(),
					30.3609,
					59.9311,
					float64(2800),
					"m").Return(orders, nil)
			},
			args: args{
				ctx:    context.Background(),
				lng:    30.3609,
				lat:    59.9311,
				radius: 2800,
				unit:   "m",
			},
			want: []models.Order{{
				ID:            1,
				Price:         123,
				DeliveryPrice: 123,
				Lng:           30.3600,
				Lat:           59.9310,
				IsDelivered:   false,
			},
				{
					ID:            2,
					Price:         321,
					DeliveryPrice: 321,
					Lng:           30.3605,
					Lat:           59.9316,
					IsDelivered:   false,
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			f := fields{
				storage:       mock_storage.NewMockOrderStorager(ctrl),
				allowedZone:   alZones,
				disabledZones: disalZones,
			}

			if tt.prepare != nil {
				tt.prepare(&f)
			}

			o := &OrderService{
				storage:       f.storage,
				allowedZone:   f.allowedZone,
				disabledZones: f.disabledZones,
			}
			got, err := o.GetByRadius(tt.args.ctx, tt.args.lng, tt.args.lat, tt.args.radius, tt.args.unit)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetByRadius() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByRadius() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOrderService_GetCount(t *testing.T) {
	type fields struct {
		storage       *mock_storage.MockOrderStorager
		allowedZone   geo.PolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		prepare func(f *fields)
		args    args
		want    int
		wantErr bool
	}{
		{
			name: "GetCount_test",
			prepare: func(f *fields) {
				f.storage.EXPECT().GetCount(context.Background()).Return(3, nil)
			},
			args:    args{ctx: context.Background()},
			want:    3,
			wantErr: false,
		},
		{
			name: "GetCount_test_error",
			prepare: func(f *fields) {
				f.storage.EXPECT().GetCount(context.Background()).Return(0, fmt.Errorf("error"))
			},
			args:    args{ctx: context.Background()},
			want:    0,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			f := fields{
				storage:       mock_storage.NewMockOrderStorager(ctrl),
				allowedZone:   alZones,
				disabledZones: disalZones,
			}

			if tt.prepare != nil {
				tt.prepare(&f)
			}

			o := &OrderService{
				storage:       f.storage,
				allowedZone:   f.allowedZone,
				disabledZones: f.disabledZones,
			}
			got, err := o.GetCount(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetCount() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetCount() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOrderService_RemoveOldOrders(t *testing.T) {
	type fields struct {
		storage       *mock_storage.MockOrderStorager
		allowedZone   geo.PolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		prepare func(f *fields)
		args    args
		wantErr bool
	}{
		{
			name: "RemoveOldOrders_test",
			prepare: func(f *fields) {
				f.storage.EXPECT().RemoveOldOrders(context.Background(), 2*time.Minute).Return(nil)
			},
			args:    args{ctx: context.Background()},
			wantErr: false,
		},
		{
			name: "RemoveOldOrders_test_error",
			prepare: func(f *fields) {
				f.storage.EXPECT().RemoveOldOrders(context.Background(), 2*time.Minute).Return(fmt.Errorf("error"))
			},
			args:    args{ctx: context.Background()},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			f := fields{
				storage:       mock_storage.NewMockOrderStorager(ctrl),
				allowedZone:   alZones,
				disabledZones: disalZones,
			}

			if tt.prepare != nil {
				tt.prepare(&f)
			}

			o := &OrderService{
				storage:       f.storage,
				allowedZone:   f.allowedZone,
				disabledZones: f.disabledZones,
			}
			if err := o.RemoveOldOrders(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("RemoveOldOrders() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestOrderService_Save(t *testing.T) {
	type fields struct {
		storage       *mock_storage.MockOrderStorager
		allowedZone   geo.PolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx   context.Context
		order models.Order
	}
	tests := []struct {
		name    string
		prepare func(f *fields)
		args    args
		wantErr bool
	}{
		{
			name: "Save_test",
			prepare: func(f *fields) {
				f.storage.EXPECT().Save(context.Background(), models.Order{
					ID:            0,
					Price:         600,
					DeliveryPrice: 600,
					Lng:           30.0,
					Lat:           30.0,
					IsDelivered:   false,
				}, 2*time.Minute+3).Return(nil)
			},
			args: args{
				ctx: context.Background(),
				order: models.Order{
					ID:            0,
					Price:         600,
					DeliveryPrice: 600,
					Lng:           30.0,
					Lat:           30.0,
					IsDelivered:   false,
				},
			},
			wantErr: false,
		},
		{
			name: "Save_test_error",
			prepare: func(f *fields) {
				f.storage.EXPECT().Save(context.Background(), models.Order{
					ID:            0,
					Price:         600,
					DeliveryPrice: 600,
					Lng:           30.0,
					Lat:           30.0,
					IsDelivered:   false,
				}, 2*time.Minute+3).Return(fmt.Errorf("error"))
			},
			args: args{
				ctx: context.Background(),
				order: models.Order{
					ID:            0,
					Price:         600,
					DeliveryPrice: 600,
					Lng:           30.0,
					Lat:           30.0,
					IsDelivered:   false,
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			f := fields{
				storage:       mock_storage.NewMockOrderStorager(ctrl),
				allowedZone:   alZones,
				disabledZones: disalZones,
			}

			if tt.prepare != nil {
				tt.prepare(&f)
			}

			o := &OrderService{
				storage:       f.storage,
				allowedZone:   f.allowedZone,
				disabledZones: f.disabledZones,
			}
			if err := o.Save(tt.args.ctx, tt.args.order); (err != nil) != tt.wantErr {
				t.Errorf("Save() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestOrderService_UpdateDelivered(t *testing.T) {
	orders := []models.Order{{
		ID:            1,
		Price:         123,
		DeliveryPrice: 123,
		Lng:           30.3600,
		Lat:           59.9310,
		IsDelivered:   false,
	},
		{
			ID:            2,
			Price:         321,
			DeliveryPrice: 321,
			Lng:           30.3605,
			Lat:           59.9316,
			IsDelivered:   false,
		},
	}
	type fields struct {
		storage       *mock_storage.MockOrderStorager
		allowedZone   geo.PolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx    context.Context
		orders []models.Order
	}
	tests := []struct {
		name    string
		prepare func(f *fields)
		args    args
		want    error
		want1   bool
	}{
		{
			name: "UpdateDelivered_test",
			prepare: func(f *fields) {
				f.storage.EXPECT().UpdateDelivered(context.Background(), orders).Return(nil, true)
			},
			args: args{
				ctx: context.Background(),
				orders: []models.Order{{
					ID:            1,
					Price:         123,
					DeliveryPrice: 123,
					Lng:           30.3600,
					Lat:           59.9310,
					IsDelivered:   false,
				},
					{
						ID:            2,
						Price:         321,
						DeliveryPrice: 321,
						Lng:           30.3605,
						Lat:           59.9316,
						IsDelivered:   false,
					},
				},
			},
			want:  nil,
			want1: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			f := fields{
				storage:       mock_storage.NewMockOrderStorager(ctrl),
				allowedZone:   alZones,
				disabledZones: disalZones,
			}

			if tt.prepare != nil {
				tt.prepare(&f)
			}

			o := &OrderService{
				storage:       f.storage,
				allowedZone:   f.allowedZone,
				disabledZones: f.disabledZones,
			}
			got, got1 := o.UpdateDelivered(tt.args.ctx, tt.args.orders)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UpdateDelivered() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("UpdateDelivered() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
