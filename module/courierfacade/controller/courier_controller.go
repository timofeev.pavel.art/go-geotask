package controller

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"time"

	"gitlab.com/timofeev.pavel.art/go-geotask/prometeus"

	"github.com/gin-gonic/gin"
	"gitlab.com/timofeev.pavel.art/go-geotask/module/courierfacade/service"
)

type CourierController struct {
	courierService service.CourierFacer
}

func NewCourierController(courierService service.CourierFacer) *CourierController {
	return &CourierController{courierService: courierService}
}

func (c *CourierController) GetStatus(ctx *gin.Context) {
	// установить задержку в 50 миллисекунд

	// получить статус курьера из сервиса courierService используя метод GetStatus
	// отправить статус курьера в ответ
	time.Sleep(time.Millisecond * 50)

	start := time.Now()

	status := c.courierService.GetStatus(ctx)
	ctx.JSON(http.StatusOK, status)

	elapsed := time.Since(start)
	prometeus.GetStatusDuration.Observe(float64(elapsed.Microseconds()))
}

func (c *CourierController) MoveCourier(m webSocketMessage) {
	var (
		cm  CourierMove
		err error
	)
	start := time.Now()
	// получить данные из m.Data и десериализовать их в структуру CourierMove

	// вызвать метод MoveCourier у courierService
	err = json.Unmarshal(m.Data.([]byte), &cm)
	if err != nil {
		log.Println(err)
	}

	c.courierService.MoveCourier(context.Background(), cm.Direction, cm.Zoom)

	elapsed := time.Since(start)
	prometeus.MoveDuration.Observe(float64(elapsed.Microseconds()))
}
