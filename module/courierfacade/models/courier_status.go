package models

import (
	cm "gitlab.com/timofeev.pavel.art/go-geotask/module/courier/models"
	om "gitlab.com/timofeev.pavel.art/go-geotask/module/order/models"
)

// swagger:model CourierStatus
type CourierStatus struct {
	Courier cm.Courier `json:"courier"`
	Orders  []om.Order `json:"orders"`
}
