package service

import (
	"context"
	"reflect"
	"testing"

	models3 "gitlab.com/timofeev.pavel.art/go-geotask/module/order/models"

	models2 "gitlab.com/timofeev.pavel.art/go-geotask/module/courier/models"

	"github.com/golang/mock/gomock"

	mock_courierservice "gitlab.com/timofeev.pavel.art/go-geotask/module/courier/service/courier_mock"
	"gitlab.com/timofeev.pavel.art/go-geotask/module/courierfacade/models"
	mock_orderservice "gitlab.com/timofeev.pavel.art/go-geotask/module/order/service/order_service_mock"
)

func TestCourierFacade_GetStatus(t *testing.T) {
	type fields struct {
		courierService *mock_courierservice.MockCourierer
		orderService   *mock_orderservice.MockOrderer
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		prepare func(f *fields)
		args    args
		want    models.CourierStatus
	}{
		{
			name: "OK",
			prepare: func(f *fields) {
				f.courierService.EXPECT().GetCourier(context.Background()).Return(&models2.Courier{
					Score: 0,
					Location: models2.Point{
						Lat: 59.9311,
						Lng: 30.3609,
					},
				}, nil)
				f.orderService.EXPECT().
					GetByRadius(context.Background(), 30.3609, 59.9311, float64(2800), "m").
					Return([]models3.Order{
						{
							ID:            1,
							Price:         200,
							DeliveryPrice: 150,
							Lng:           30.3600,
							Lat:           59.9311,
							IsDelivered:   false,
						},
						{
							ID:            2,
							Price:         200,
							DeliveryPrice: 250,
							Lng:           30.36010,
							Lat:           59.93,
							IsDelivered:   false,
						},
					}, nil)
				f.orderService.EXPECT().
					GetByRadius(context.Background(), 30.3609, 59.9311, float64(5), "m").
					Return([]models3.Order{
						{
							ID:            1,
							Price:         200,
							DeliveryPrice: 150,
							Lng:           30.3600,
							Lat:           59.9311,
							IsDelivered:   false,
						},
					}, nil)
				f.orderService.EXPECT().UpdateDelivered(context.Background(), []models3.Order{
					{
						ID:            1,
						Price:         200,
						DeliveryPrice: 150,
						Lng:           30.3600,
						Lat:           59.9311,
						IsDelivered:   false,
					},
				}).Return(nil, true)
				f.courierService.EXPECT().UpdateCourier(context.Background()).Return(nil)
			},
			args: args{ctx: context.Background()},
			want: models.CourierStatus{
				Courier: models2.Courier{
					Score: 0,
					Location: models2.Point{
						Lat: 59.9311,
						Lng: 30.3609,
					},
				},
				Orders: []models3.Order{
					{
						ID:            1,
						Price:         200,
						DeliveryPrice: 150,
						Lng:           30.3600,
						Lat:           59.9311,
						IsDelivered:   false,
					},
					{
						ID:            2,
						Price:         200,
						DeliveryPrice: 250,
						Lng:           30.36010,
						Lat:           59.93,
						IsDelivered:   false,
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			f := fields{
				courierService: mock_courierservice.NewMockCourierer(ctrl),
				orderService:   mock_orderservice.NewMockOrderer(ctrl),
			}

			if tt.prepare != nil {
				tt.prepare(&f)
			}

			c := &CourierFacade{
				courierService: f.courierService,
				orderService:   f.orderService,
			}
			if got := c.GetStatus(tt.args.ctx); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetStatus() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCourierFacade_MoveCourier(t *testing.T) {
	type fields struct {
		courierService *mock_courierservice.MockCourierer
		orderService   *mock_orderservice.MockOrderer
	}
	type args struct {
		ctx       context.Context
		direction int
		zoom      int
	}
	tests := []struct {
		name    string
		prepare func(f *fields)
		args    args
	}{
		{
			name: "OK",
			prepare: func(f *fields) {
				f.courierService.EXPECT().GetCourier(context.Background()).Return(&models2.Courier{
					Score: 0,
					Location: models2.Point{
						Lat: 59.9311,
						Lng: 30.3609,
					},
				}, nil)
				f.courierService.EXPECT().MoveCourier(models2.Courier{
					Score: 0,
					Location: models2.Point{
						Lat: 59.9311,
						Lng: 30.3609,
					},
				}, 0, 6).Return(nil)
			},
			args: args{
				ctx:       context.Background(),
				direction: 0,
				zoom:      6,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			f := fields{
				courierService: mock_courierservice.NewMockCourierer(ctrl),
				orderService:   mock_orderservice.NewMockOrderer(ctrl),
			}

			if tt.prepare != nil {
				tt.prepare(&f)
			}

			c := &CourierFacade{
				courierService: f.courierService,
				orderService:   f.orderService,
			}
			c.MoveCourier(tt.args.ctx, tt.args.direction, tt.args.zoom)
		})
	}
}
