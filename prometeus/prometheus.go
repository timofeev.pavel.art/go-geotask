package prometeus

import "github.com/prometheus/client_golang/prometheus"

var (
	MoveDuration = prometheus.NewHistogram(prometheus.HistogramOpts{
		Name: "move_duration_microseconds",
		Help: "Продолжительность выполнения запроса MoveCourier",
	})

	GetStatusDuration = prometheus.NewHistogram(prometheus.HistogramOpts{
		Name: "get_status_duration_microseconds",
		Help: "Продолжительность выполнения запроса GetStatus",
	})

	RepositoryMethodsDuration = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name: "repos_method_duration",
		Help: "Продолжительность выполнения методов репозитория",
	}, []string{"repository", "method"})
)

func RegisterPrometheusMetrics() {
	prometheus.MustRegister(MoveDuration, GetStatusDuration, RepositoryMethodsDuration)
}
