// nolint:all
package docs

import "gitlab.com/timofeev.pavel.art/go-geotask/module/courierfacade/models"

// добавить документацию для роута /api/status

// swagger:route GET /api/status getstatus getStatusCourierRequest
// Get status
// responses:
//	200: getStatusCourierResponse

// swagger:parameters getStatusCourierRequest
type getStatusCourierRequest struct {
}

// swagger:response getStatusCourierResponse
type getStatusCourierResponse struct {
	// in:body
	Body models.CourierStatus
}
